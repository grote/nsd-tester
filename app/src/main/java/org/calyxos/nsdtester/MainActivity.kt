package org.calyxos.nsdtester

import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.MutableStateFlow
import org.calyxos.nsdtester.ui.theme.NSDTesterTheme

private const val SERVICE_TYPE = "_googlecast._tcp."

/**
 * https://developer.android.com/training/connect-devices-wirelessly/nsd
 */
class MainActivity : ComponentActivity(), NsdManager.DiscoveryListener {

    companion object {
        private const val TAG = "TEST"
        private const val mServiceName = "_googlecast._tcp."
    }

    private val nsdManager by lazy { getSystemService(NsdManager::class.java) }

    private val state = MutableStateFlow<DiscoveryState>(DiscoveryState.Stopped)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NSDTesterTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    val s = state.collectAsState()
                    StateComposable(s)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        nsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, this)

    }

    override fun onStop() {
        super.onStop()
        nsdManager.stopServiceDiscovery(this)
    }

    // Called as soon as service discovery begins.
    override fun onDiscoveryStarted(regType: String) {
        Log.e(TAG, "Service discovery started")
        state.value = DiscoveryState.DiscoveryStarted
    }

    override fun onServiceFound(service: NsdServiceInfo) {
        // A service was found! Do something with it.
        Log.e(TAG, "Service discovery success $service")
        state.value = DiscoveryState.ServiceFound(service)
        when {
            service.serviceType != SERVICE_TYPE -> Log.d(TAG,
                "Unknown Service Type: ${service.serviceType}")
            service.serviceName == mServiceName -> Log.d(TAG, "Found: $mServiceName")
        }
        nsdManager.resolveService(service,
            object : NsdManager.ResolveListener {
                override fun onResolveFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
                    Log.e(TAG, "onResolveFailed: $errorCode")
                }

                override fun onServiceResolved(serviceInfo: NsdServiceInfo) {
                    Log.e(TAG, "onServiceResolved: $serviceInfo")
                    state.value = DiscoveryState.ServiceResolved(serviceInfo)
                }
            }
        )
    }

    override fun onServiceLost(service: NsdServiceInfo) {
        // When the network service is no longer available.
        // Internal bookkeeping code goes here.
        Log.e(TAG, "service lost: $service")
        state.value = DiscoveryState.ServiceLost(service)
    }

    override fun onDiscoveryStopped(serviceType: String) {
        Log.e(TAG, "Discovery stopped: $serviceType")
        state.value = DiscoveryState.Stopped
    }

    override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
        Log.e(TAG, "Discovery failed: Error code:$errorCode")
        nsdManager.stopServiceDiscovery(this)
        state.value = DiscoveryState.StartFailed(errorCode)
    }

    override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
        Log.e(TAG, "Discovery failed: Error code:$errorCode")
        nsdManager.stopServiceDiscovery(this)
        state.value = DiscoveryState.StopFailed(errorCode)
    }

}

sealed class DiscoveryState {
    object Stopped : DiscoveryState()
    object DiscoveryStarted : DiscoveryState()
    data class StartFailed(val errorCode: Int) : DiscoveryState()
    data class StopFailed(val errorCode: Int) : DiscoveryState()
    data class ServiceFound(val service: NsdServiceInfo) : DiscoveryState()
    data class ServiceLost(val service: NsdServiceInfo) : DiscoveryState()
    data class ServiceResolved(val service: NsdServiceInfo) : DiscoveryState()
}

@Composable
fun StateComposable(state: State<DiscoveryState>) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .fillMaxHeight(),
        contentAlignment = Alignment.Center
    ) {
        when (val s = state.value) {
            is DiscoveryState.Stopped -> {
                Text(text = "Discovery stopped!")
            }
            is DiscoveryState.DiscoveryStarted -> {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    LinearProgressIndicator()
                    Text(text = "Looking for $SERVICE_TYPE", Modifier.padding(16.dp))
                    Text(text = "If it doesn't find anything for some time, try disabling and re-enabling your Wi-Fi.",
                        Modifier.padding(16.dp))
                }
            }
            is DiscoveryState.ServiceFound -> {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Icon(Icons.Default.Check, null, tint = Color.Yellow)
                    Text(text = "Found ${s.service.serviceName}", Modifier.padding(16.dp))
                    Text(text = "Now getting details...", Modifier.padding(16.dp))
                }
            }
            is DiscoveryState.ServiceResolved -> {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Icon(Icons.Default.Check, null, tint = Color.Green)
                    Text(text = "Found ${s.service.serviceName} at ${s.service.host}:${s.service.port}",
                        Modifier.padding(16.dp))
                }
            }
            else -> {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Icon(Icons.Default.Warning, null, tint = Color.Red)
                    Text(text = "$s", Modifier.padding(16.dp))
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    NSDTesterTheme {
        val state = remember { mutableStateOf(DiscoveryState.Stopped) }
        StateComposable(state)
    }
}
